# CKEditor Text Transformation / AutoCorrect

This module adds the [automatic text transformation](https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html)
feature to CKEditor 5.

The 1.x branch works with CKEditor 4 and integrates the [AutoCorrect](https://ckeditor.com/cke4/addon/autocorrect)
plugin, which provides similar function.

## Post Installation and Setup (2.x / CKEditor 5)

After installing this module, the **TextTransformation** plugin is automatically enabled on all your CKE5 editors.

There's no UI to tweak the plugin settings yet, but you can implement `hook_ckeditor5_plugin_info_alter()` in a custom
module and change the available transformations. For example:

```php
function MYMODULE_ckeditor5_plugin_info_alter(array &$plugin_definitions) {
  if (isset($plugin_definitions['ckeditor_text_transformation_textTransformation'])) {
    $plugin = $plugin_definitions['ckeditor_text_transformation_textTransformation']->toArray();
    $plugin['ckeditor5']['config']['typing']['transformations'] = [
      'remove' => ['arrowLeft', 'arrowRight'],
      'extra' => [
        ['from' => ':)', 'to' => '🙂']
      ],
    ];
    $plugin_definitions['ckeditor_text_transformation_textTransformation'] = new Drupal\ckeditor5\Plugin\CKEditor5PluginDefinition($plugin);
  }
}
```

Check the [plugin page](https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html#configuring-transformations)
to find more info about the configuration options.
